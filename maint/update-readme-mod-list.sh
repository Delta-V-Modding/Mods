#!/bin/bash
set -eEuo pipefail
shopt -s lastpipe

cd "$(dirname "$0")"/..

declare -A nicknames
nicknames[Vladimir Panteleev]=CyberShadow
nicknames[Alfik]=Alfik0

# Sort by date added
find mods -mindepth 1 -maxdepth 1 -type d -printf '%f\n' |
	while read -r mod
	do
		date=$(git log --format=%ai -- mods/"$mod" |
			tail -1)
		printf '%s\t%s\n' "$date" "$mod"
	done |
	sort |
	cut -d $'\t' -f 2 |
	mapfile -t mods

{
	sed '/^| Name/,$d'
	printf '| %-40s | %-15s | %-80s |\n' Name Author Summary
	printf '| %-40s | %-15s | %-80s |\n' '' '' '' | tr ' ' '-'
	for mod in "${mods[@]}"
	do
		name_and_link="[$mod](mods/$mod)"
		author=$(git log --format=%an -- mods/"$mod" | tail -1)
		if [[ -z "$author" ]]
		then
			author=$(git config user.name)
		fi
		if [[ -v "nicknames[$author]" ]]
		then
			author=${nicknames[$author]}
		fi
		summary=$(tail -n +4 mods/"$mod"/README.md | head -1)
		printf '| %-40s | %-15s | %-80s |\n' "$name_and_link" "$author" "$summary"
	done
} < README.md > README.md.new
mv README.md{.new,}
