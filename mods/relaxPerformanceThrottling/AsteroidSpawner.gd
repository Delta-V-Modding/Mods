extends "res://AsteroidSpawner.gd"

func getValidatedFloatFromConfig(config:ConfigFile, seg:String, setting:String) -> float:
	var val = config.get_value(seg, setting)
	var type = typeof(val)
	if ((type == TYPE_INT || type == TYPE_REAL) && val >= 0):
		return (val as float)
	else:
		return -1.0

func _init():
	var modName = "relaxPerformanceThrottling"
	var configPath = "user://modConfig.txt"
	var defaults = {"throttleMax": 0.0, "collisionPairsMult":100.0, "asteroidDespawnMinBoosted":true}
	Debug.l(modName+": Reading config")
	var config = ConfigFile.new()

	var f = File.new()
	f.open(configPath,File.READ)
	var err = config.load(configPath)
	f.close()

	if err != OK || !config.has_section(modName):
		Debug.l(modName+": Creating config")
		f.open(configPath, File.WRITE)
		for setting in defaults:
			config.set_value(modName, setting, defaults[setting])
		config.save(configPath)
		f.close()
		return

	throttleMax = getValidatedFloatFromConfig(config, modName, "throttleMax")
	if throttleMax < 0:
		throttleMax = defaults["throttleMax"]

	var collisionPairsMult: float = getValidatedFloatFromConfig(config, modName, "collisionPairsMult")
	if collisionPairsMult < 1:
		collisionPairsMult = defaults["collisionPairsMult"]
	throttleCollisionPairsStart *= collisionPairsMult
	throttleCollisionPairs *= collisionPairsMult

	var astDespawnBoost: bool = getValidatedFloatFromConfig(config, modName, "asteroidDespawnMinBoosted")
	if (typeof(astDespawnBoost) == TYPE_BOOL && astDespawnBoost):
		despawnRadiusAsteroidsLoad *= 2
		if despawnRadiusAsteroidsLoad > despawnRadiusAsteroids:
			despawnRadiusAsteroidsLoad = despawnRadiusAsteroids - 100
