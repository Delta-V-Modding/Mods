extends Node

func _init(modLoader = ModLoader):
	var modName = "relaxPerformanceThrottling"
	Debug.l(modName+": Initializing")
	modLoader.installScriptExtension("res://"+modName+"/AsteroidSpawner.gd")
	Debug.l(modName+": Finished initialization")