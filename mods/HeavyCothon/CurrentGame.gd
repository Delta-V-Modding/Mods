extends "res://CurrentGame.gd"

func _init():
	# Allow HEAVY-COTHON to appear on sale as an used ship
	usedShipsPool = usedShipsPool + [
		{"name":"HEAVY-COTHON", "age":24 * 3600 * 365 * 200}
	]

# Add a brand new HEAVY-COTHON at the top of the dealer sale ship list
func getShipsAvailableForSale():
	var now = getInGameTimestamp()
	var day = int(floor(now / (24 * 3600)))
	var week = int(day / 7) * 7 + 500000.0

	var ships = []
	ships.append(createShipInstanceWithCache("HEAVY-COTHON", 24 * 3600 * 7, "HEAVY-COTHON".hash() + week, true))
	ships.append_array(.getShipsAvailableForSale())
	return ships
