extends "res://ships/ship-ctrl.gd"

func isInEscapeCondition():
	# The maximum speed in the game logic is hard-coded;
	# instead, temporarily adjust the linear_velocity vector
	# so the check does what we want.
	var ORIGINAL_MAX_SPEED = 2000
	var MAX_SPEED = 400 * 10

	var original_linear_velocity = linear_velocity
	linear_velocity /= (MAX_SPEED / ORIGINAL_MAX_SPEED)
	var result = .isInEscapeCondition()
	linear_velocity = original_linear_velocity
	return result
