AlertSounds
===========

Plays different alert sounds depending on the cause.

New sounds are played in the following situations:

- A collision is imminent (for autopilots with that functionality).
- An indicator is in the red (e.g. due to low fuel).

Other causes will play the default HUD sound, as before.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/AlertSounds.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

To use other sounds, replace the `.pcm` files in the mod `.zip` file. The format of the `.pcm` files is 16-bit signed mono, 44100Hz.
