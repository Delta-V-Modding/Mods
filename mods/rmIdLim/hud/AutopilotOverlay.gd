extends "res://hud/AutopilotOverlay.gd"

func getValidatedFloatFromConfig(config:ConfigFile, seg:String, setting:String) -> float:
	var val = config.get_value(seg, setting)
	var type = typeof(val)
	if ((type == TYPE_INT || type == TYPE_REAL) && val >= 0):
		return (val as float)
	else:
		return -1.0

func _init():
	var modName = "rmIdLim"
	var configPath = "user://modConfig.txt"
	var defaults = {"chunksMarkerMultiplier": 100.0, "tacticalMarkerMultiplier":100.0}
	Debug.l(modName+": Reading config")
	var config = ConfigFile.new()

	var f = File.new()
	f.open(configPath,File.READ)
	var err = config.load(configPath)
	f.close()

	if err != OK || !config.has_section(modName):
		Debug.l(modName+": Creating config")
		f.open(configPath, File.WRITE)
		for setting in defaults:
			config.set_value(modName, setting, defaults[setting])
		config.save(configPath)
		f.close()
		return
		
	var chunksMarkerMultiplier : float = getValidatedFloatFromConfig(config, modName, "chunksMarkerMultiplier")
	if chunksMarkerMultiplier < 1:
		chunksMarkerMultiplier = defaults["chunksMarkerMultiplier"]

	var tacticalMarkerMultiplier : float = getValidatedFloatFromConfig(config, modName, "tacticalMarkerMultiplier")
	if tacticalMarkerMultiplier < 1:
		tacticalMarkerMultiplier = defaults["tacticalMarkerMultiplier"]

	Debug.l("rmIdLim: Changing limits")
	mineralMarkerMax *= chunksMarkerMultiplier
	tacticalMarkerMax *= tacticalMarkerMultiplier
