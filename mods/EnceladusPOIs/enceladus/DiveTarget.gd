extends "res://enceladus/DiveTarget.gd"

var _enceladusPOIs
var _enceladusPOIs_lut = []
var _enceladusPOIs_timer

func _ready():
	_enceladusPOIs = ItemList.new()

	var theme = load("res://hud/TNTRL-theme-inventory.tres")
	_enceladusPOIs.theme = theme
	_enceladusPOIs.set("custom_styles/bg", StyleBoxEmpty.new())
	_enceladusPOIs.set("custom_styles/bg_focus", StyleBoxEmpty.new())

	_enceladusPOIs.connect("item_selected", self, "_enceladusPOIs_selected")

	add_child(_enceladusPOIs)

	_enceladusPOIs_timer = Timer.new()
	_enceladusPOIs_timer.connect("timeout", self, "_enceladusPOIs_update")
	add_child(_enceladusPOIs_timer)
	_enceladusPOIs_timer.start()
	_enceladusPOIs_update()

func _enceladusPOIs_update():
	var textLines = []
	_enceladusPOIs_lut = []
	var now = CurrentGame.getInGameTimestamp()
	var destinations = CurrentGame.getCourseDestinations()
	for d in destinations:
		var destination = destinations[d]
		if destination.time == null:
			textLines.append(tr(d))
		else:
			var time = Tool.readableTimeSpan(CurrentGame.getTimeLeftForDestination(destination))
			textLines.append("%s [%s]" % [tr(d.split("|")[0]), time])
		_enceladusPOIs_lut.append(d)

	# A hack to avoid a small visual annoyance (removing selection every second).
	# Instead of clearing and repopulating the list, we update existing items,
	# also adjusting the size to fit.
	while _enceladusPOIs.get_item_count() < textLines.size():
		_enceladusPOIs.add_item("")
	while _enceladusPOIs.get_item_count() > textLines.size():
		_enceladusPOIs.remove_item(0)
	for i in range(textLines.size()):
		_enceladusPOIs.set_item_text(i, textLines[i])

	_enceladusPOIs.anchor_left = 0.75
	_enceladusPOIs.anchor_right = 1
	_enceladusPOIs.margin_left = 0
	_enceladusPOIs.margin_right = -16

	_enceladusPOIs.auto_height = true
	_enceladusPOIs.anchor_top = 1
	_enceladusPOIs.anchor_bottom = 1
	_enceladusPOIs.margin_top = 0
	_enceladusPOIs.margin_bottom = -100
	_enceladusPOIs.grow_vertical = Control.GROW_DIRECTION_BEGIN

func _enceladusPOIs_selected(idx):
	var currentDestination = _enceladusPOIs_lut[idx]
	var dest = CurrentGame.getCourseDestinations().get(currentDestination)
	var vector = dest.vector / 10000
	vector -= computeFreeOffset()
	emit_signal("changeTrajectory", vector)
