TogglePropulsion
===========

Adds key binds to toggle all thrusters or main engines.

Download
--------

The latest version can be downloaded from [GitLab CI artifacts](https://gitlab.com/Delta-V-Modding/Mods/-/jobs/artifacts/main/file/mods/TogglePropulsion.zip?job=zip).

Usage
-----

See the [modding documentation](../../MODDING.md) for general information.

To set the keybinds, open `settings.cfg` in your ΔV user folder. There, you can add or set the keys:

```gdscript
toggle_rcs=[ "F" ]
toggle_engines=[ "G" ]
```

Starting the game once with the mod will create the lines with no keybindings.
